/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
	var war = 16;
    generateDiscs(war);
    solveHanoi(war, 1, 2, 3);
});


function generateDiscs(number){
    number = number || 3;
    
    var template = $('#disc-template').html();
    var firstStick = $('#gamepanel .stick.col-1');
    for(var i = 0; i < number; i++){
        var currentDisc = $(template);
        console.log(firstStick);
        currentDisc.attr('data-value', i);
        currentDisc.css("background-color", generateRandomColor());
        currentDisc.css("width", 100 + (i * 25));
        currentDisc.appendTo(firstStick);
    }
    console.log("Discs generated: " + number );
}

function move(startStickNr, endStickNr){
    
    var startStick = $('#gamepanel .stick.col-' + startStickNr);
    var endStick = $('#gamepanel .stick.col-' + endStickNr);
    
    var firstDisc = startStick.children(':first');
    var secondDisc = endStick.children(':first');
    
    if(secondDisc.length !== 0) {
        if(firstDisc.length === 0){
            console.error('No disc to take from stick ' + startStickNr);
            return false;
        }
        if(parseInt(firstDisc.attr('data-value'), 10) > parseInt(secondDisc.attr('data-value'), 10)){
            console.error('Cannot move first disc of stick ' + startStickNr + '(' + firstDisc.attr('data-value') + ') to stick ' + endStickNr + '(' + secondDisc.attr('data-value') + ')');
            return false;
        }
    }
    
    firstDisc.remove();
    firstDisc.prependTo(endStick);
    
}

function generateRandomColor(){
    return '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
}

function solveHanoi(i, stick1, stick2, stick3, callback) {
    if(i > 0) {
        solveHanoi(i-1, stick1, stick3, stick2, function() {
            window.setTimeout(function() {
                if(move(stick1, stick3) === false) {
                    return;
                }
                solveHanoi(i-1, stick2, stick1, stick3, callback);
            }, 1);
        });

    } else {
        if(callback) callback();
    }

}